
// Практика: 
// 1 ************************************************************


// const danItStudent = {
//   name: "Yaroslav",
//   lastName: "Hrochev",
//   readyHW: 7,
// }

// const question = prompt('Що ви зочете дізнатись про студента?');

// if (question === 'lastname') {
//   console.log(danItStudent.name) 
// } else if (question === "lastName") {
//   console.log(danItStudent.lastName)
// } else if (question === "readyHW") {
//   console.log(danItStudent.readyHW)
// } else {
//   console.log('Вибачте таких данних немає')
// }


// const danItStudent = {
//   name: "Yaroslav",
//   lastName: "Hrochev",
//   readyHW: 7,
//   task: 0,

//   getInfo(question) {
//     return this.hasOwnProperty(question) ? this[question] : "Вибачте таких данних немає";
//   }
// }

// const question = prompt('Що ви зочете дізнатись про студента?');

// console.log(danItStudent.getInfo(question));


//2 ************************************************************

// const createUser = (firstName, lastName) => {
//   const user = {
//     firstName,
//     lastName,
//     // fullName() {
//     //   return `${this.firstName} ${this.lastName}`
//     // }

//     get fullName() {
//       return `${this.firstName} ${this.lastName}`
//     },

//     setFirstName(newFirstName) {
//       Object.defineProperty(this, 'firstName', {
//         writable: true,
//       })

//       this.newFirstName = firstName;

//       Object.defineProperty(this, 'firstName', {
//         writable: false,
//       })
//     }
//   };


//   Object.defineProperties(user, {
//     firstName: {
//       writable: false,
//     },
//     lastName: {
//       writable: false,
//     }
//   });



//   return user
// }

// const newUser = createUser('Sam', 'Jonson');
// console.log(newUser);
// console.log(newUser.fullName);

//3 ************************************************************

// Створити об'єкт danItStudent,
// У якого такі властивості: ім'я, прізвище, кількість зданих домашніх работ.
// Вивести об'єкт у консоль.
// Після чого спочатку запитати у користувача "Яку властивість ви хочете змінити?",
// Потім - "На яке значення?". Змінити потрібну властивість і вивести об'єкт у консоль.


// const danItStudent = {
//   name: "Yaroslav",
//   lastName: "Hrochev",
//   readyHW: 7,

//   getInfo() {
//     return this[question] = prompt('На яке значення?');
//   }
// }

// const question = prompt('Яку властивість ви хочете змінити?');

// if (danItStudent.hasOwnProperty(question)) {
//     danItStudent.getInfo()
// } else {
//     alert('You have a mistakes')
// }

// console.log(danItStudent)


//4 ************************************************************

// Запитати користувача ім'я, прізвище, вік (перевірити на коректну цифру
// і що число між 15 і 100), чи є діти, якого кольору очі.
// Усі ці дані записати в об'єкт.
// Перебрати об'єкт і вивести у консолі фразу
// `Властивість ${name}: ${значення name}` і так з усіма властивостями

// // // const validateString = string => !(string === null || string === "");
// const validateString = string => !(!string);
// // // const validateNumber = number => !(number === null || number === "" || isNaN(number)); 
// const validateNumber = number => !(!number || isNaN(number)); 

// const getNumber = (message = "Enter your number", validator = validateNumber) => {
//     let userNumber;
//     do {
//         userNumber = prompt(message);
//     } while (!validator(userNumber) )

//     // || userNumber > 15|| userNumber > 100
//     return +userNumber;
// }

// const getUserName = (message = "Enter your name") => {
//     let userName;
//     do {
//         userName = prompt(message);
//     } while (!validateString(userName))
//     return userName;
// }

// const newUser = {
//     name: getUserName('Введите ваше имя'),
//     lastName: getUserName('Введите вашу фамилию'),
//     age: getNumber('Введите ваш возвраст', (age) => validateNumber(age) && age > 15 && age < 100),
//     ayes: getUserName('Якого кольору у вас очі'),
//     children: confirm("чи є у вас діти?"),
// }

// const showObjectInfo = (obj) => {
//     for (let key in obj) {
//         console.log(`Властивість ${key} : ${obj[key]}`)
//     } 
// }

// showObjectInfo(newUser);


//5 ************************************************************

// Запитати в користувача його ім'я, прізвище, вік.
// З цими даними створити об'єкт person
// за допомогою функції попереднього завдання,
// Доповнивши об'єкт методом sayHi, який виводитиме в консоль
// `Привіт, я ${властивість ім'я}, ${властивість прізвище}, мені ${властивість вік}`.
// У створеного об'єкта, викликати метод sayHi;

// // const validateString = string => !(string === null || string === "");
// const validateString = string => !(!string);
// // const validateNumber = number => !(number === null || number === "" || isNaN(number)); 
// const validateNumber = number => !(!number || isNaN(number)); 

// const getUserName = (message = "Enter your name") => {
//     let userName;
//     do {
//         userName = prompt(message);
//     } while (!validateString(userName))
//     return userName;
// }

// const getNumber = (message = "Enter your number") => {
//     let userNumber;
//     do {
//         userNumber = prompt(message);
//     } while (!validateNumber(userNumber))
//     return +userNumber;
// }

// const createUser = () => {
//     const person = {
//         name: getUserName('Введите ваше имя'),
//         lastName: getUserName('Введите вашу фамилию'),
//         age: getNumber('Введите ваш возвраст'),

//         getGreeting() {
//             // const info = `Привіт, я ${this.name} ${this.lastName}, мені ${this.age}`;
//             return `Привіт, я ${this.name} ${this.lastName}, мені ${this.age}`;
//         }
//     }

//     return person
// }

// let person = createUser();
// console.log(person.getGreeting());


/**
//    6 ************************************************************
 *
 * За допомогою циклу for...in вивести в консоль усі властивості
 * першого рівня об'єкта у форматі «ключ-значення».
 *
 * Просунута складність:
 * Поліпшити цикл так, щоб він умів виводити властивості об'єкта другого рівня вкладеності.
 */

// const danItStudent = {
//     name: "Yaroslav",
//     lastName: "Hroshev",
//     readyHW: 7,
//     age: 29,
//     task: 0,
//     hobby: {
//         game: "tennis",
//         reading: "book",
//         additionalHobby: {
//             gamesdf: "sdfsdfsdfsdf",
//             readingsdfsdf: "bosdfsdfsdfok"
//         },
//     },
//     a: null,
//     getInf(question) {
//         return this.hasOwnProperty(question) ? this[question] : "Вибачте таких даних немає"
//     }
// }


// // for (let key in danItStudent) {
// //     if (typeof danItStudent[key] === 'object' && danItStudent[key] !== null) {

// //         for (let secondaryKey in danItStudent[key]) {
// //             console.log(`${secondaryKey} - ${danItStudent[key][secondaryKey]}`) // danItStudent.hobby.game, danItStudent.hobby.reading
// //         } 
// //         continue

// //     } 
// //     console.log(`${key} - ${danItStudent[key]}`)
     
// // }

// const showObjectInfo = (obj) => {
    
//     for (let key in obj) {
//         if (typeof obj[key] === 'object' && obj[key] !== null) {
//             showObjectInfo(obj[key]);
//         } else {
//             console.log(`${key} - ${obj[key]}`);
//         }
 
//     }
// }  

// showObjectInfo(danItStudent);


//  ADVACED  7 ************************************************************


// **
//  * Завдання.
//  *
//  * Написати функцію-помічник для ресторану.
//  *
//  * Функція має два параметри:
//  * - Розмір замовлення (small, medium, large);
//  * - Тип обіду (breakfast, lunch, dinner).
//  *
//  * Функція повертає об'єкт із двома полями:
//  * - totalPrice - загальна сума страви з урахуванням її розміру та типу;
//  * - totalCalories — кількість калорій, що міститься у блюді з урахуванням його розміру та типу.
//  *
//  * Нотатки:
//  * - Додаткові перевірки робити не потрібно;
//  * - У рішенні використовувати референтний об'єкт з цінами та калоріями.
//  */

/* Дано */
const priceList = {
  sizes: {
    small: {
      price: 15,
      calories: 250,
    },
    medium: {
      price: 25,
      calories: 340,
    },
    large: {
      price: 35,
      calories: 440,
    },
  },
  types: {
    breakfast: {
      price: 4,
      calories: 25,
    },
    lunch: {
      price: 5,
      calories: 5,
    },
    dinner: {
      price: 10,
      calories: 50,
    },
  },
};

// 8 ************************************************************


/* Написати імплементацію методу Object.freeze().
  *
  * Метод повинен:
  * - запобігати можливості додавання нових властивостей до об'єкта;
  * - запобігати можливості видалення існуючих властивостей об'єкта;
  * - запобігати можливості зміни дескрипторів властивостей об'єкта;
  * - Запобігати можливості зміни значення властивостей об'єкта.
  *
  * Умови:
  * - Вбудованим методом Object.freeze() користуватися заборонено;
*/


