// пример  по split и map для разделения рядка в масив и наоборот
// const str = 'my-long-word';

// function camelize(str) {
//   return str
//     .split('-') // розбиваємо 'my-long-word' на масив елементів ['my', 'long', 'word']
//     .map(
//       // робимо першу літеру велику для всіх елементів масиву, крім першого
//       // конвертуємо ['my', 'long', 'word'] в ['my', 'Long', 'Word']
//       (word, index) => index == 0 ? word : word[0].toUpperCase() + word.slice(1)
//     )
//     .join(''); // зʼєднуємо ['my', 'Long', 'Word'] в 'myLongWord'
// }


// const a = camelize(str);
// console.log(a)



// пример  по filter

// let arr = [5, 3, 8, 1];

// let filtered = filterRange(arr, 1, 4);

// function filterRange(arr, a, b) {
//   // навколо виразу додано дужки для кращої читабельності
//   return arr.filter(item => (a <= item && item <= b));
// }


// console.log( filtered ); // 3,1 (відфільтровані значення)

// console.log( arr );


// пример  по map

// let ivan = { name: "Іван", surname: "Іванко", id: 1 };
// let petro = { name: "Петро", surname: "Петренко", id: 2 };
// let mariya = { name: "Марія", surname: "Мрійко", id: 3 };

// let users = [ ivan, petro, mariya ];

// let usersMapped = users.map(user => ({
//   fullName: `${user.name} ${user.surname}`,
//   id: user.id
// }));

// /*
// usersMapped = [
//   { fullName: "Іван Іванко", id: 1 },
//   { fullName: "Петро Петренко", id: 2 },
//   { fullName: "Марія Мрійко", id: 3 }
// ]
// */

// alert( usersMapped[0].id ); // 1
// alert( usersMapped[0].fullName ); // Іван Іванко


// пример  по sort

// function sortByAge(arr) {
//   arr.sort((a, b) => a.age - b.age);
// }

// let ivan = { name: "Іван", age: 25 };
// let petro = { name: "Петро", age: 30 };
// let mariya = { name: "Марія", age: 28 };

// let arr = [ petro, ivan, mariya ];

// sortByAge(arr);

// // тепер відсортовано: [ivan, mariya, petro]
// alert(arr[0].name); // Ivan
// alert(arr[1].name); // Mariya
// alert(arr[2].name); // Petro


// пример  по sort

// function getAverageAge(users) {
//   return users.reduce((prev, user) => prev + user.age, 0) / users.length;
// }

// let ivan = { name: "Іван", age: 25 };
// let petro = { name: "Петро", age: 30 };
// let mariya = { name: "Марія", age: 29 };

// let arr = [ ivan, petro, mariya ];

// alert( getAverageAge(arr) ); // 28



// 1********************************************************************************************************

// Створити масив об'єктів students (у кількості 7 прим).
// У кожного студента має бути ім'я, прізвище та напрямок навчання - Full-stask
// чи Front-end. Кожен студент повинен мати метод, sayHi, який повертає рядок
// `Привіт, я ${ім'я}, студент Dan, напрям ${напрямок}`.
// Перебрати кожен об'єкт і викликати у кожного об'єкта метод sayHi;

// const students = [
//     { name: 'Bob', 
//       learn: 'Front-end', 
//       sayHi() {
//         return `Привіт, я ${this.name}, студент, напрям ${this.learn}`
//       }
//     },

//     { name: 'Rob', 
//         learn: 'Full-stask', 
//         sayHi() {
//         return `Привіт, я ${this.name}, студент, напрям ${this.learn}`
//         },
//     },

//     { name: 'Den', 
//         learn: 'Front-end', 
//         sayHi() {
//         return `Привіт, я ${this.name}, студент, напрям ${this.learn}`
//         },
//     },

    
//     { name: 'Ed', 
//         learn: 'Full-stask', 
//         sayHi() {
//         return `Привіт, я ${this.name}, студент, напрям ${this.learn}`
//         },
//     },


//     { name: 'Frank', 
//         learn: 'Front-end', 
//         sayHi() {
//         return `Привіт, я ${this.name}, студент, напрям ${this.learn}`
//         },
//     },
// ]

// students.forEach((elem, index) => {
//     console.log(elem.sayHi())
// })



// 2*******************************************************************************************************

// Є масив брендів автомобілів ["bMw", "Audi", "teSLa", "toYOTA"].
// Вам потрібно отримати новий масив, об'єктів типу
// {
//   type: 'car'
//   brand: ${елемент масиву}
// }

// Вивести масив у консоль

// const cars = ["bMw", "Audi", "teSLa", "toYOTA"];

// const getNewCars = (arr) => {
//     let newCars = [];
//     for (let i = 0; i < arr.length; i++) {
//         newCars.push({type: 'car', brand: arr[i]});
//     }
//     return newCars
// }

// const arrCars = getNewCars(cars);
// console.log(arrCars)

// arrCars.forEach((elem) => {
//     if (elem.brand !== "teSLa") {
//         elem.єElectric = false;
//     } else {
//         elem.єElectric = true;
//     }
// });

// console.log(arrCars)

// вариант 2 правильный:

// const carBrand = ["bMw", "Audi", "teSLa", "toYOTA"];

// const checkIsElectricCar = (car) => {
//     const electricCars = ['tesla', 'bmw', 'toyota', 'audi'];

//     return electricCars.includes(car.toLowerCase())
// }

// const newCar = carBrand.map(element => {

//     return {
//         type: 'car',
//         brand: element,
//         electric: checkIsElectricCar(element)
//     }
// })

// console.log(newCar)

// const electricCar = newCar.filter(element => {
//     return element.electric === true
// })

// console.log(electricCar)



// 3*******************************************************************************************************

// Створити масив чисел від 1 до 100.
// Відфільтрувати його таким чином, щоб до нового масиву не потрапили числа менше 10 і більше 50.
// Вивести у консоль новий масив.

// const numbers = [];
// numbers[0] = 1;
// for (let i = 0; i < 99; i++) {
//     numbers.push(numbers[i] + 1);
// }

// console.log(numbers)

// const newNumbers = numbers.filter((elem) => {
//     return elem > 10 && elem < 50 
// });

// console.log(newNumbers)


// 4*******************************************************************************************************

// Створити функцію, яка перевіряє, чи є переданий у неї рядок
// Паліндром (анна це паліндром).

// Підказка: потрібно перетворити рядок на масив


// const word = prompt('Enter word');

// варіант1 
// const isPolindrom = (str) => str.split('').reverse().join('') === str;
// console.log(isPolindrom(word))

// варіант2
// const arr = word.split('');
// const reverceArr = arr.reverse();
// const word2 = reverceArr.join('');

// const checkPalindr = (first, two) => {
//     if (first === two) {
//         return 'Це Паліндр';
//     }
//     return 'Це не Паліндр';
// }

// const palindr = checkPalindr(word, word2);
// console.log(palindr)


// 4*******************************************************************************************************
// Опис завдання: Напишіть функцію, яка очищає масив від небажаних значень,
// Таких як false, undefined, порожні рядки, нуль, null.

// const data = [0, 1, false, 2, undefined, '', 3, null];
// console.log(compact(data)) // [1, 2, 3]

// const data = [0, 1, false, 2, undefined, '', 3, null];


// const compact = arr => arr.filter(elem => elem);
// // const compact = (arr) => {
// //     return arr.filter((elem) => {
// //         return elem 
// //     })
    
// // }

// console.log(compact(data)) // [1, 2, 3]
    


// 5*******************************************************************************************************

// Опис завдання: Напишіть функцію, яка порівнює два масиви і повертає true,
// якщо вони ідентичні.
// Очікуваний результат: ([1, 2, 3], [1, 2, 3]) => true

// const arr1 = [1, 2, 3, 4];
// const arr2 = [1, 2, 3, 4];
// const arr3 = [1, 2, 3, 5];
// const arr4 = [1, 2, 3, 4, 5];
// console.log(isEqual(arr1, arr2)); // true
// console.log(isEqual(arr1, arr3)); // false
// console.log(isEqual(arr1, arr4)); // false



const groceryShop = {

  getCategory(){
    const productsKeys = Object.keys(this.products).sort((a, b) => a.localeCompare(b)).join(', ') 
    
    return `Категорії: ${productsKeys}`
  },
  
  getProductString(){
    const productsStringArr = this.allProducts.map(element => element.name).sort((a, b) => a.localeCompare(b))
    return ` Продукти : ${productsStringArr.join(", ")}`
  },

  get allProducts() {
    const value = Object.values(this.products);
    return [].concat(...value)
  },

  getProductByName(productName) {
    const product = this.allProducts.find(elem => {
      return productName === elem.name
    })
    if (!product) {
      alert(`Товара з такою назвою ${productName} немає`)
      return
    } 
    return product
  },

  getSortedProducts(key = "price", sortingType = "ascending") {
    if (sortingType === "ascending") {
      return this.allProducts.sort((a, b) => a[key] - b[key])
    }

    if (sortingType === "descending") {
      return this.allProducts.sort((a, b) => b[key] - a[key])
    }

    console.warn("Wrong sorting type");
    return this.allProducts

  },

  getProductsByCity(city) {
    return this.allProducts.filter((elem) => {
      return elem.cities.includes(city);
    }) 
  },

  // order(...args) {
  //   const totalSum = args.reduce((acc, elem) => {

  //     const name = elem[0]
  //     let quantity = elem[1]

  //     const product = this.getProductByName(name)
  //     console.log(product)

  //     if (!product || product.quantity === 0) {
  //       alert(`Продукту ${name} немає`);
  //       return acc
  //     }
  //     if (product.quantity < quantity) {
  //       alert(`Продукту ${name} залишилось ${product.quantity}`);
  //       quantity = product.quantity;
  //     }

  //     return acc += product.price * quantity
  //   }, 0)


  // order(...args) {
  //   let totalSum = 0;

  //   args.forEach((elem) => {

  //     const name = elem[0];
  //     let quantity = elem[1];

  //     const product = this.getProductByName(name)
  //     console.log(product)

  //     if (!product || product.quantity === 0) {
  //       alert(`Продукту ${name} немає`);
  //       return;
  //     }
  //     if (product.quantity < quantity) {
  //       alert(`Продукту ${name} залишилось ${product.quantity}`);
  //       quantity = product.quantity;
  //     }

  //     totalSum += product.price * quantity

      
  //   }, 0)

// console.log(totalSum)
  

    products: {
      fruits: [
        {
          name: "banana",
          quantity: 69,
          price: 893.28,
          cities: ["Kyiv", "Odesa", "Dnipro"]
        },
        {
          name: "green banana",
          quantity: 22,
          price: 254.28,
          cities: ["Kyiv", "Dnipro"]
        },
        {
          name: "grapes",
          quantity: 61,
          price: 609.19,
          cities: ["Kyiv", "Odesa"]
        },
        {
          name: "lime",
          quantity: 62,
          price: 560.01,
          cities: ["Odesa", "Dnipro"]
        },
        {
          name: "lemon",
          quantity: 0,
          price: 128.46,
          cities: ["Kyiv", "Dnipro"]
        },
        {
          name: "blueberry",
          quantity: 0,
          price: 872.17,
          cities: ["Kyiv", "Odesa", "Lviv", "Dnipro"]
        },
        {
          name: "mini banana",
          quantity: 45,
          price: 1200.65,
          cities: ["Kyiv", "Lviv"]
        },
        {
          name: "apple",
          quantity: 10,
          price: 379.64,
          cities: ["Kyiv", "Odesa"]
        },
      ],
      bakery: [
        {
          name: "bread",
          quantity: 42,
          price: 539.74,
          cities: ["Kyiv", "Odesa", "Lviv"]
        },
        {
          name: "dark bread",
          quantity: 22,
          price: 839.74,
          cities: ["Kyiv"]
        },
        {
          name: "cupcake",
          quantity: 19,
          price: 206.63,
          cities: ["Kyiv", "Odesa"]
        },
        {
          name: "pie",
          quantity: 35,
          price: 170.11,
          cities: ["Kyiv", "Dnipro"]
        },
        {
          name: "bagel",
          quantity: 0,
          price: 881.49,
          cities: ["Kyiv", "Odesa", "Lviv"]
        },
      ],
      fish: [
        {
          name: "shark",
          quantity: 91,
          price: 470.27,
          cities: ["Kyiv", "Odesa", "Dnipro"]
        },
        {
          name: "tuna",
          quantity: 90,
          price: 441.92,
          cities: ["Kyiv"]
        },
        {
          name: "salmon",
          quantity: 33,
          price: 832.63,
          cities: ["Lviv", "Dnipro"]
        },
      ],
      meat: [
        {
          name: "chicken",
          quantity: 35,
          price: 46.809,
          cities: ["Kyiv", "Odesa", "Dnipro"]
        },
        {
          name: "beef",
          quantity: 59,
          price: 190.01,
          cities: ["Kyiv", "Lviv", "Dnipro"]
        },
        {
          name: "turkey",
          quantity: 0,
          price: 2.39,
          cities: ["Kyiv", "Odesa", "Lviv", "Dnipro"]
        },
      ]
    }
  };

