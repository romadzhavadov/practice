По кліку на картинку - збільшити її в 2 рази.

По кліку на будь-який з параграфів - збільшити всім параграфам шрифт - зробити 40 пікселів. Якщо натиснути ще раз - зменшити до вихідної величини.

По кліку на кнопку - Замінити текст всередині неї на 'Clicked';