/**
 * Завдання 1.
 *
 * Отримати та вивести в консоль такі елементи сторінки:
 * - за ідентифікатором (id): елемент з ідентифікатором list;
 * - за класом - елементи з класом list-item;
 * - За тегом - елементи з тегом li;
 * - За CSS селектором (один елемент) - третій li зі всього списку;
 * - За CSS селектором (багато елементів) – всі доступні елементи li.
 *
 * Вивести в консоль і пояснити властивості елемента:
 * - innerText;
 * - innerHTML;
 * - outerHTML.
 */


// const list = document.getElementById('list');
// const listItem = document.getElementsByClassName('list-item');
// const li = document.getElementsByTagName('li');
// const cssLi = document.querySelector('ul li:nth-child(3)');
// const cssAllLi = document.querySelectorAll('li');


// console.log(list);
// console.log(listItem);
// console.log(li);
// console.log(cssLi);
// console.log(cssAllLi);


// 2_____________________________

// const removeElement = document.querySelector('.remove');

// // removeElement.classList.remove('remove');

// // removeElement.style.display = "none";

// // removeElement.style.display = "none";

// removeElement.remove();

// console.log(removeElement)


// const bigger = document.querySelector('.bigger');
// console.log(bigger)

// // bigger.className = 'active';

// bigger.classList.replace('bigger', 'active');


// 3_____________________________


const listElements = document.querySelectorAll('li');

listElements.forEach((elem) => {
    console.log(elem.innerText.split(': ')[1]);
    if (elem.innerText.split(': ')[1] === '0') {
        elem.innerText = elem.innerText.replace('0', 'закінчився');
        elem.style.color = 'red';
        elem.classList.add('bold')
        
    }

})

console.log(listElements);

